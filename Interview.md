

# Interview

> Steps to setup the env and execute the scripts
> 


### Install Cypress

* Go to https://www.cypress.io/
* Download cypress tool directly or using npm : $npm install cypress --save -dev


### Using the project

* Unzip the interview folder 
* via cmd open the application: interview/node_modules/.bin/cypress open or if you installed the application directly downloading the app select the project folder 
* After that it should open the cypress runner
* ![] (/Users/admin/Desktop/Screen Shot 2019-05-17 at 2.08.58 PM.png)
* select the sample_spec.js file and click on "Run all specs"


