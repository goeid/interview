describe('Test number one',function(){
	it('Opens google.com and searches',function(){
		cy.visit('https://www.google.com/?gl=us')
		cy.get('input.gLFyf.gsfi')
		.type('Bahamas')
		cy.contains('Google Search')
		.click()
		cy.get('#rcnt').contains('Bahamas')
		cy.screenshot('results-Bahamas')
		cy.get('input.gLFyf.gsfi')
		.clear()
		.type('amsterdam')
		cy.get('button.Tg7LZd')
		.click()
		cy.get('#rcnt').contains('amsterdam')
		cy.screenshot('results-amsterdam')



	})

})



describe('TEST API',function(){
	it('Openweather call',function(){
		cy.request('https://api.openweathermap.org/data/2.5/weather?q=new%20york,us&mode=json&appid=4fc07e9a556d69f6370398e330e46781')
		.then((response) => {
				expect(response.body.main).to.have.property('temp_max')
				expect(response.body.main.temp_max).to.be.lessThan(10)
			})
	})
})
